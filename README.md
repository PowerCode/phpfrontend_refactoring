# phpfrontend_refactoring

You will find 2 files in this repo: a Controller and a view file. The Controller does a lot more than what is used within the vue file. 
Both files have grown over time, and are in need for a refactoring. The goal of this test task is to see how you would refactor the files within 1h of your time, given the following ground rules:

* The method signatures MUST NOT change. You need to be able to allow for backwards compatibility
* We follow the clean-code principle: extract methods for hard-to-understand code sections with proper names, and use variables with good names extensively. If you're doing it well, you will not need any Python-comments. Your code should be readable like a book - also by someone who is not a Python coder (but who has coding experience)
* Write your code in the `develop` branch and submit a Merge Request to us once you're done. In case of questions, add a ticket to your GitLab Issue board and assign @patrick
* Do NOT change the order of the methods / the spaces in the files. We assess your work by looking at the text diff, and if you move stuff around / reformat whitespaces, every line appears changed. *We only want to see the lines you actually changed!*
* In case you see that your Merge Request marks lots of stuff as changed where you didn't really improve that much, you can also opt to create an entirely new file that ONLY contains methods that you have changed

# How-to
1. fork this repo into your personal / company gitlab account (wherevery you like it to be)
1. add Maintainer-permission to Patrick and Michael (see the Members tab to get details)
1. stark working on the `develop` branch of your repo. Stop after 1h. You don't need to be done after 1h, but be sure that you spend your time wisely
1. create a MR merging develop into master and assign yourself to track your progress and ensure that the textdiff is showing only things that you want us to see rather than lots of whitespace. Set it to work-in-progress to signal that you're not done with it yet
1. Once you're done, remove the work-in-progress flag of the MR, assign patrick and write a comment into the MR detailing your thoughts on your refactoring